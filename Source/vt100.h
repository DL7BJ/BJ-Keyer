/** @file   vt100.h
 *  @brief  Headerdatei für vt100.c
 */
#ifndef VT100_H_INCLUDED
#define VT100_H_INCLUDED
 
#include "functions.h"

#define SERMENUE        1
#define SERMSG          2
#define SERMSGMENUE     3
#define SERMSGCHANGE    4
#define SERMSG1         5
#define SERMSG2         6
#define SERMSG3         7
#define SERMSG4         8
#define SERMSG5         9
#define SERMSG0         0
#define SERSETUP        10

void SerialInfo(void);
void SerialAbout(void);
void SerialSetup(void);
void SerialMenue(void);
void SerialMsgOut(void);
void SerialMsgMenue(char c);
// Externals
extern void WriteMsgEEprom(uint8_t idx);

// Stringkonstanten für VT100 Terminals
static const char* const CLRSCR PROGMEM = "\033[2J";        ///<    Display löschen

char out[120];                                              ///< Ausgabebuffer
char inp[120];                                              ///< Eingabebuffer

uint8_t i;                                                  ///< akt. Position Eingabebuffer
uint8_t o;                                                  ///< akt. Position Ausgabebuffer

// Status der seriellen Schnittstelle
struct SerialState
{
    uint8_t Ausgabe;                                        ///< aktives Menue oder aktive Ausgabe
    uint8_t Eingabe;                                        ///< Eingabe Textspeicher
};

struct SerialState bSerialState;                            ///< Struktur für Variablen der seriellen Schnittstelle

#endif
