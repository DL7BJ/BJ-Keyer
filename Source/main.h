/** @file   main.h
 *  @brief  Headerdatei für main.c
 *
 * 
 * 
 */
#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include "globals.h"
#include "encoder.h"
#include "functions.h"
#include "controls.h"
#include "vt100.h"

struct Config bConfig;          ///< Struktur für die Einstellungen
struct MenuCtrl bMenuCtrl;      ///< Struktur für die Menuesteuerung
struct State bState;            ///< Struktur für den aktuellen Status
struct Message bMessage;        ///< Struktur für die 5 Textspeicher
// Function prototypes
void Init(void);
void InitTimer(void);
void ReStart(void);

extern void CheckDahPaddle(void);
extern void CheckDitPaddle(void);
extern void CheckStraightKey(void);
extern void CheckButtons(void);
extern void SetFrequency(uint16_t f);
extern void DitDahBuffers(void);
extern void SetStandardMsg(void);
extern void DahPaddle(void);
extern void DitPaddle(void);
extern void ReadMsgEEprom(uint8_t i);
extern void SerialReceive(char c);
extern void SerialReset(void);
#endif // BJ-KEYER_H_INCLUDED
