/** @file   functions.h
 *  @brief  Headerdatei für functions.c
 *
 */ 
#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#include "main.h"

uint8_t sreg_tmp;                           ///< Merker für Registerinhalte

// Prototypes
void SymbolLoop(uint8_t Len);
void CheckPaddles(void);
void SetRatio(void);
void SetWeight(void);
void SendDit(void);
void SendDah(void);
void SendIambic(void);
void SendChar(unsigned char c);
void SendString(char *s);
// Sidetone
void SideToneOn(void);
void SideToneOff(void);
void Tone(uint16_t f, uint8_t duration);
void Boop(void);
void Beep(void);
void BeepBoop(void);
// Interrupt
void IntDisable(void);
void IntEnable(void);
void DisablePinChangeIRQ(void);
void EnablePinChangeIRQ(void);
// EEPROM
void WriteEEprom(void);
void ReadEEprom(void);
void WriteEEpromWpM(void);
void ReadEEpromWpM(void);
void SetEEprom(void);
// Serielle Schnittstelle
void SerialWriteString(char *s);
void SerialWriteChar(unsigned char data);
// Delay Millis
void DelayMilliSeconds(uint16_t ms);
void ResetMilliSeconds(void);
uint16_t GetMilliSeconds(void);
// Externals
extern void SendMemory(uint8_t ButtonPressed);
#endif
