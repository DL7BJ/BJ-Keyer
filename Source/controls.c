/** @file   controls.c
 *  @brief  Display und Bedienelemente
 *
 *          In dieser Datei sind alle Funktionen für die 
 *          Ausgabe auf dem OLED Dispay und die Bedienelemente
 *
 */

#include "controls.h"

// Stringkonstanten für das Display
static const char* const sTrx1 PROGMEM = "TRX 1";                   ///< OLED Text für Transceiver 1
static const char* const sTrx2 PROGMEM = "TRX 2";                   ///< OLED Text für Transceiver 2
static const char* const IambicA PROGMEM = "Iambic A";              ///< OLED Text für Iambic A
static const char* const IambicB PROGMEM = "Iambic B";              ///< OLED Text für Iambic B
static const char* const Ultimatic PROGMEM = "Ultimat.";            ///< OLED Text für Ultimatic
static const char* const Memory PROGMEM = "Memory";                 ///< OLED Text für Punkt/Strich Speicher
static const char* const Ratio PROGMEM = "Ratio";                   ///< OLED Text für Ratio
static const char* const Weight PROGMEM = "Weight";                 ///< OLED Text für Gewichtung
static const char* const ReverseRL PROGMEM = " L -   R °";          ///< OLED Text für Links Dag, Rechts Dit
static const char* const ReverseLR PROGMEM = " L °   R -";          ///< OLED Text für Links Dit, Rechts Dah
static const char* const SideToneOnOff PROGMEM = "Mithörton";       ///< OLED Text für Mithörton An/Aus
static const char* const SideToneFreq PROGMEM = "Frequenz";         ///< OLED Text für Frequenz des Tons
static const char* const RiseTime PROGMEM = "Anstieg";              ///< OLED Text für Anstiegszeit Mithörton
static const char* const DebounceTime PROGMEM = "Entprl.";          ///< OLED Text für Zeitintervall Entprellung
static const char* const Yes PROGMEM = "J";                         ///< OLED Text für Ja
static const char* const No PROGMEM = "N";                          ///< OLED Text für Nein
static const char* const Hz PROGMEM = "Hz";                         ///< OLED Text für Frequenz in Hertz
static const char* const Einstellungen PROGMEM = "Einstellungen";   ///< OLED Text für Einstellungen    
static const char* const Ton PROGMEM = "Ton";                       ///< OLED Text für Ton
static const char* const TonAn PROGMEM = "Ton an";                  ///< OLED Text für Ton an
static const char* const TonAus PROGMEM = "Ton aus";                ///< OLED Text für Ton aus
static const char* const MemAus PROGMEM = "Mem AUS";                ///< OLED Text für Punkt/Strich Speicher aus
static const char* const MemAn PROGMEM = "Mem EIN";                 ///< OLED Text für Punkt/Strich Speicher an

/** @fn     Drehencoder
 *  @brief  Mit dieser Funktion wird per Polling der Drehencoder abgefragt
 *
 *          Es wird die Stellung des Encoders und der Status des Tasters 
 *          abgefragt. Die Daten werden ausgewertet und die entsprechende
 *          Funktion aufgerufen
 *
 *  @param  none
 *  @retval none
 */
void Drehencoder(void)
{
    int st = 0;
    static int last;

    if(!(bMenuCtrl.Config))
    {
        EncoderMinMax(5,50);
        st = EncoderRead(1);
        if(bConfig.WpM != st)
        {
            bConfig.WpM = st;           //! neuen Wert in bConfig speichern
            bState.WpMChanged = 1;      //! Status für Timer 0 setzen
            bMenuCtrl.Update = 1;       //! Status für UpdateDisplay setzen
            EncoderPos = st;            //! neuen Werte in EncoderPos speichern
            WpM = bConfig.WpM;          //! neuen Wert in WpM übernehmen
            cli();
            StoreEEpromTimer = 0;       //! Timer 0 Variable löschen für Update EEprom
            sei();
            SetRatio();
            SetWeight();
        }
    }

    if((bMenuCtrl.buttonPressed == 1) && (bMenuCtrl.Config == 0))
    {
        bMenuCtrl.Config = 1;
        MenuCtrlTimer = 0;
        bMenuCtrl.buttonPressed = 0;
        EncoderPos = EncoderRead(1);
        bMenuCtrl.CurMenue = EncoderPosConfig;
        EncoderWrite(EncoderPosConfig);
        bMenuCtrl.Update = 1;
    }

    if((bMenuCtrl.buttonPressedLong == 1) && (bMenuCtrl.Config == 1))
    {
        if(bMenuCtrl.SubMenue == 1)
        {
            bMenuCtrl.SubMenue = 0;
            bMenuCtrl.Update = 1;
            bMenuCtrl.buttonPressedLong = 0;
            bMenuCtrl.m_buttonPressed = 0;
            EncoderWrite(EncoderPosSubConfig);
            Boop();
        } else {
            bMenuCtrl.Config = 0;
            bMenuCtrl.Update = 1;
            bMenuCtrl.buttonPressedLong = 0;
            bMenuCtrl.buttonPressed = 0;
            bMenuCtrl.m_buttonPressed = 0;
            MenuCtrlTimer = 0;
            EncoderPosConfig = EncoderRead(1);
            EncoderWrite(EncoderPos);
            bMenuCtrl.ClrScr = 1;
            WriteEEprom();
            Beep();
        }
    }

    if(bMenuCtrl.Config == 1)
    {
        if((!bMenuCtrl.buttonPressed) && (bMenuCtrl.SubMenue == 0))
        {
            EncoderMinMax(1,M_MAX);
            st = EncoderRead(1);
            if(last != st)
            {
                bMenuCtrl.CurMenue = st;
                bMenuCtrl.Update = 1;
            }
            last = st;
        }

        if(bMenuCtrl.buttonPressed)
        {
            bMenuCtrl.m_buttonPressed = 1;
            bMenuCtrl.buttonPressed = 0;
        }

        if((bMenuCtrl.m_buttonPressed == 1) && (bMenuCtrl.SubMenue == 0))
        {
            UpdateDisplay();
            bMenuCtrl.Update = 1;
            switch(bMenuCtrl.CurMenue)
            {
                case M_TRX1:
                    if(bConfig.Trx1)
                        bConfig.Trx1 = 0;
                    else
                        bConfig.Trx1 = 1;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_TRX2:
                    if(bConfig.Trx2)
                        bConfig.Trx2 = 0;
                    else
                        bConfig.Trx2 = 1;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_IAMBICA:
                    bConfig.KeyerMode = IAMBIC_A;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_IAMBICB:
                    bConfig.KeyerMode = IAMBIC_B;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_ULTIMATIC:
                    bConfig.KeyerMode = ULTIMATIC;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_REVERSE:
                    if(bConfig.Reverse == 1)
                        bConfig.Reverse = 0;
                    else
                        bConfig.Reverse = 1;
                    if(bConfig.Reverse == 1)
                        PaddleMode = PADDLE_REVERSE;
                    else
                        PaddleMode = PADDLE_NORMAL;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_RATIO:
                    bMenuCtrl.SubMenue = 1;
                    EncoderPosSubConfig = EncoderRead(1);
                break;
                case M_WEIGHT:
                    bMenuCtrl.SubMenue = 1;
                    EncoderPosSubConfig = EncoderRead(1);
                break;
                case M_MEMORY:
                    bConfig.Memory = (bConfig.Memory == 1) ? 0 : 1;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_WPMBPM:
                    bConfig.WpMBpM = (bConfig.WpMBpM == 1) ? 0 : 1;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_TON:
                    if(bConfig.SidetoneEnabled == 1)
                        SidetoneDisable();
                    else 
                        SidetoneEnable();
                    bMenuCtrl.m_buttonPressed = 0;
                break;
                case M_TON_FREQ:
                    bMenuCtrl.SubMenue = 1;
                    EncoderPosSubConfig = EncoderRead(1);
                break;
                case M_RISETIME:
                    bMenuCtrl.SubMenue = 1;
                    EncoderPosSubConfig = EncoderRead(1);
                break;
                case M_DEBOUNCE:
                    bMenuCtrl.SubMenue = 1;
                    EncoderPosSubConfig = EncoderRead(1);
                break;
                case M_MEMBUTTONMODE:
                    bConfig.MemButtonMode = (bConfig.MemButtonMode == 1) ? 0 : 1;
                    bMenuCtrl.m_buttonPressed = 0;
                break;
            }
        }
        // Einstellungen für variable Werte
        if((bMenuCtrl.m_buttonPressed == 1) && (bMenuCtrl.SubMenue == 1))
        {
            UpdateDisplay();
            switch(bMenuCtrl.CurMenue)
            {
                case M_RATIO:
                    EncoderMinMax(15,45);
                    EncoderWrite(bConfig.Ratio);
                    st = EncoderRead(1);
                    if(st != bConfig.Ratio)
                        bMenuCtrl.Update = 1;
                    bConfig.Ratio = st; 
                    if(bConfig.Ratio > 40) bConfig.Ratio = 45;
                    if(bConfig.Ratio < 15) bConfig.Ratio = 15;
                    SetRatio();
                break;
                case M_WEIGHT:
                    EncoderMinMax (25,75);
                    EncoderWrite(bConfig.Weight);
                    st = EncoderRead(1);
                    if(st != bConfig.Weight)
                        bMenuCtrl.Update = 1;
                    bConfig.Weight = st;
                    if(bConfig.Weight > 75) bConfig.Weight = 75;
                    if(bConfig.Weight < 25) bConfig.Weight = 25;
                    SetWeight();
                break;
                case M_TON_FREQ:
                    EncoderMinMax(30,100);
                    EncoderWrite(bConfig.SidetoneFreq/10);
                    st = EncoderRead(1);
                    if(st != bConfig.SidetoneFreq/10)
                    {
                        bConfig.SidetoneFreq = st * 10;
                        if(bConfig.SidetoneFreq > 1000) bConfig.SidetoneFreq = 1000;
                        if(bConfig.SidetoneFreq < 300) bConfig.SidetoneFreq = 300;
                        bMenuCtrl.Update = 1;
                        Tone(bConfig.SidetoneFreq, 250);
                    }
                break;
                case M_RISETIME:
                    EncoderMinMax(1,10);
                    EncoderWrite(bConfig.RiseTime);
                    st = EncoderRead(1);
                    if(st != bConfig.RiseTime)
                    {
                        bConfig.RiseTime = st;
                        if(bConfig.RiseTime > 10) bConfig.RiseTime = 10;
                        if(bConfig.RiseTime < 1) bConfig.RiseTime = 1;
                        bMenuCtrl.Update = 1;
                    }
                break;
                case M_DEBOUNCE:
                    EncoderMinMax(1,25);
                    EncoderWrite(bConfig.DebounceTime);
                    st = EncoderRead(1);
                    if(st != bConfig.DebounceTime)
                    {
                        bConfig.DebounceTime = st;
                        if(bConfig.DebounceTime > 25) bConfig.DebounceTime = 25;
                        if(bConfig.DebounceTime < 1) bConfig.DebounceTime = 1;
                        bMenuCtrl.Update = 1;
                    }
                break;
            }
        }
   }
}
/** @fn     void ConfigMenue(void)
 *  @brief  Ausgabe des Menues für die Einstellungen
 *
 *  @param  none
 *  @retval none
 */
void ConfigMenue(void)
{
    char line[22];
    lcd_clrscr();
    lcd_charMode(NORMAL);
    lcd_gotoxy(0,0);
    lcd_puts(CLEARLINE);
    lcd_gotoxy(0,0);
    if(bMenuCtrl.SubMenue == 0)
        sprintf(line,"%s    - %i",Einstellungen, bMenuCtrl.CurMenue);
    else
        sprintf(line,"%s    * %i",Einstellungen, bMenuCtrl.CurMenue);
    lcd_puts(line);
    lcd_charMode(DOUBLESIZE);
    lcd_gotoxy(0,3);
    lcd_puts(CLEARLINE);
    switch(bMenuCtrl.CurMenue)
    {
        case M_TRX1:
            lcd_gotoxy(0,3);
            if(bConfig.Trx1)
                sprintf(line,"[%s]", sTrx1);
            else
                sprintf(line," %s ", sTrx1);
            lcd_puts(line);
        break;
        case M_TRX2:
            lcd_gotoxy(0,3);
            if(bConfig.Trx2)
                sprintf(line,"[%s]", sTrx2);
            else
                sprintf(line," %s ", sTrx2);
            lcd_puts(line);
        break;
        case M_IAMBICA:
            lcd_gotoxy(0,3);
            if(bConfig.KeyerMode == IAMBIC_A)
                sprintf(line,"[%s]", IambicA);
            else 
                sprintf(line," %s ", IambicA);
            lcd_puts(line);
        break;
        case M_IAMBICB:
            lcd_gotoxy(0,3);
            if(bConfig.KeyerMode == IAMBIC_B)
                sprintf(line,"[%s]", IambicB);
            else
                sprintf(line," %s ", IambicB);
            lcd_puts(line);
        break;
        case M_ULTIMATIC:
            lcd_gotoxy(0,3);
            if(bConfig.KeyerMode == ULTIMATIC)
                sprintf(line,"[%s]", Ultimatic);
            else
                sprintf(line," %s ", Ultimatic);
            lcd_puts(line);
        break;
        case M_REVERSE:
            lcd_gotoxy(0,3);
            if(bConfig.Reverse == 0)
                sprintf(line,"%s", ReverseLR);
            else
                sprintf(line,"%s", ReverseRL);
            lcd_puts(line);
        break;
        case M_RATIO:
            lcd_gotoxy(0,3);
            sprintf(line,"%s %.1f", Ratio, (float)bConfig.Ratio/10);
            lcd_puts(line);
        break;
        case M_WEIGHT:
            lcd_gotoxy(0,3);
            sprintf(line,"%s %i",Weight,bConfig.Weight);
            lcd_puts(line);
        break;
        case M_MEMORY:
            lcd_gotoxy(0,3);
            if(bConfig.Memory == 1)
                sprintf(line,"[%s]", Memory);
            else
                sprintf(line," %s ", Memory);
            lcd_puts(line);
        break;
        case M_TON_FREQ:
            lcd_gotoxy(0,3);
            sprintf(line,"%s %uHz", Ton, bConfig.SidetoneFreq);
            lcd_puts(line);
        break;
        case M_TON:
            lcd_gotoxy(0,3);
            if(bConfig.SidetoneEnabled == 1)
                sprintf(line,"%s", TonAn);
            else
                sprintf(line,"%s", TonAus);
            lcd_puts(line);
        break;
        case M_WPMBPM:
            lcd_gotoxy(0,3);
            if(bConfig.WpMBpM == 0)
                sprintf(line,"%s", "[WpM] BpM");
            else
                sprintf(line,"%s", "WpM [BpM]");
            lcd_puts(line);
        break;
        case M_RISETIME:
            lcd_gotoxy(0,3);
            sprintf(line,"%s %ims",RiseTime, bConfig.RiseTime);
            lcd_puts(line);
        break;
        case M_DEBOUNCE:
            lcd_gotoxy(0,3);
            sprintf(line,"%s %ims",DebounceTime, bConfig.DebounceTime);
            lcd_puts(line);
        break;
        case M_MEMBUTTONMODE:
            lcd_gotoxy(0,3);
            if(bConfig.MemButtonMode == 1)
                sprintf(line,"[%s]","TRX Sw.");
            else
                sprintf(line,"[%s]","Textsp.");
            lcd_puts(line);
        break;
    }
    bMenuCtrl.Update = 0;
    lcd_charMode(NORMAL);
}
/** @fn     void DisplayVersion(void)
 *  @brief  Ausgabe der Softwareversion des BJ-Keyers
 *
 *  @param  none
 *  @retval none
 */
void DisplayVersion(void)
{
    lcd_init(LCD_DISP_ON);
    lcd_charMode(DOUBLESIZE);
    lcd_home();
    lcd_puts(PRG);
    lcd_gotoxy(1,2);
    lcd_puts(VER);
    lcd_gotoxy(2,4);
    lcd_puts(CALL);
    SidetoneOff();
    DelayMilliSeconds(1000);
}
/** @fn     void UpdateDisplay(void)
 *  @brief  UpdateDisplay
 *
 *          Aktualisierung der Anzeigen auf dem Display je nach
 *          aktueller Funktion.
 *
 *  DOUBLESIZE 4x10 character
 *  NORMALSIZE 8x21 character
*/
void UpdateDisplay(void)
{
    char line[22];
    if(bMenuCtrl.Update)
    {
        if(bMenuCtrl.ClrScr)
        {
            lcd_clrscr();
            bMenuCtrl.ClrScr = 0;
            bMenuCtrl.Update = 1;
        }
        if(!(bMenuCtrl.Config))
        {
            lcd_charMode(DOUBLESIZE);
            lcd_gotoxy(4,3);
            if(bConfig.WpMBpM == 0)
                sprintf(line,"%i WpM ",bConfig.WpM);
            else
                sprintf(line,"%i BpM ", bConfig.WpM*5);
            lcd_puts(line);
            lcd_charMode(NORMAL);
            lcd_gotoxy(13,0);
            if(bConfig.KeyerMode == IAMBIC_A)
                sprintf(line,"%s", IambicA);
            if(bConfig.KeyerMode == IAMBIC_B)
                sprintf(line,"%s", IambicB);
            if(bConfig.KeyerMode == ULTIMATIC)
                sprintf(line,"%s", Ultimatic);
            lcd_puts(line);
            lcd_gotoxy(0,0);
            if(bConfig.Trx1 == 1)
                sprintf(line, "%s", sTrx1);
            else
                sprintf(line, "      ");
            lcd_puts(line);
            lcd_gotoxy(6,0);
            if(bConfig.Trx2 == 1)
                sprintf(line, "%s", sTrx2);
            else
                sprintf(line, "      ");
            lcd_puts(line);
            lcd_gotoxy(0,6);
            sprintf(line, "R 1:%.1f",(float)bConfig.Ratio/10); 
            lcd_puts(line);
            lcd_gotoxy(9,6);
            sprintf(line, "W %i", bConfig.Weight);
            lcd_puts(line);
            lcd_gotoxy(14,6);
            if(bConfig.Memory)
                sprintf(line,MemAn);
            else
                sprintf(line,MemAus);
            lcd_puts(line);
            

        }
        if(bMenuCtrl.Config)
        {
            ConfigMenue();
        }
    }
}

