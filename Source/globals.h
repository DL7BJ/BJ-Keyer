/** @file   globals.h
 *  @brief  Globale Variablen, Definitionen und Prototypen
 *                        
 */

#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED
  
#define F_CPU           16000000UL
#define PRESCALER       8
#define SINEWAVELENGTH  64
#define F_CPUPRESIN     (F_CPU/(PRESCALER*SINEWAVELENGTH))
#define USART_BAUDRATE  9600
#define UBRR_VALUE      (((F_CPU/(USART_BAUDRATE*16UL)))-1)
  
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
  
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <util/atomic.h>

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit)) // clear bit
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))  // set bit

#include "oled/i2c.h"
#include "oled/lcd.h"
#include "oled/font.h"

#ifndef EEMEM
#define EEMEM __attribute__ ((section (".eeprom")))
#endif
  
#ifndef NOINIT
#define NOINIT __attribute__ ((section (".noinit")))
#endif

#define CALL  "  DL7BJ   "
#define PRG   " BJ-Keyer "
#define VER   "  V1.03   "

//! Keying states
#define NOTHING                   0
#define DIT_DAH_OFF               1
#define DAH_DIT_OFF               2
#define DIT_DAH_ON                3
#define DAH_DIT_ON                4
//! Keyer mode
#define STRAIGHT                  0
#define IAMBIC_B                  1
#define IAMBIC_A                  2
#define ULTIMATIC                 3
#define SINGLE_PADDLE             4
//! Paddle Mode
#define PADDLE_NORMAL             0
#define PADDLE_REVERSE            1
//! Sending Mode
#define SENDING_NOTHING           0
#define SENDING_DIT               1
#define SENDING_DAH               2
#define SENDING_SPC               3
#define SENDING_STRAIGHT          4
#define SENDING_AUTOMATIC         5
//! Sending Type
#define AUTO                      0         ///< Automatische Pausen zwischen Elementen
#define MAN                       1         ///< Manuelle Pausen zwischen Elementen
//! Ports
#define LEFT_PADDLE               PD2       ///< Left Paddle Input
#define RIGHT_PADDLE              PD3       ///< Right Paddle Input
#define STRAIGHT_KEY              PD4       ///< Straight key Input
#define MORSE_LED                 PC3       ///< LED Morse Output
#define MEM1                      PD5       ///< Mem 1 Input
#define MEM2                      PD6       ///< Mem 2 Input
#define MEM3                      PD7       ///< Mem 3 Input
#define MEM4                      PC0       ///< Mem 4 Input
#define MEM5                      PB5       ///< Mem 5 Input
#define TRX1                      PC1       ///< TRX1 Output
#define TRX2                      PC2       ///< TRX2 Output
#define AUDIO                     PB3       ///< PWM Audio Output
#define AUDIO_EN                  PB4       ///< Audio PA Enable
#define SCL                       PC5       ///< I²C LC Display
#define SDA                       PC4       ///< I²C LC Display
// States
#define ON                       1
#define OFF                      0
#define DIT                      1
#define DAH                      0
#define NO_KEY_PRESSED           0
#define KEY_PRESSED_DEBOUNCE     1
#define KEY_PRESSED              2
// Menue und Drehencoder
#define M_TRX1                   1
#define M_TRX2                   2
#define M_TON                    3
#define M_TON_FREQ               4
#define M_IAMBICA                5
#define M_IAMBICB                6
#define M_ULTIMATIC              7
#define M_MEMORY                 8
#define M_REVERSE                9
#define M_RATIO                 10
#define M_WEIGHT                11
#define M_WPMBPM                12
#define M_MEMBUTTONMODE         13
#define M_RISETIME              14
#define M_DEBOUNCE              15
#define M_MAX                   15          ///< maximale Menuepunke
//! OLED
#define CLEARLINE               "                     "
#define NORMAL                  0

struct State
{
    uint8_t WpMChanged:  1;                 ///< Geschwindigkeit geändert
    uint8_t WriteWpMEEprom: 1;              ///< nicht gespeicherte Geschwindigkeit 
    uint8_t WriteEEprom: 1;                 ///< nicht gespeicherte EEprom Werte
    uint8_t WriteMsgEEprom: 2;              ///< nicht gespeicherte Textnachrichten
    uint8_t SidetoneOff: 1;                 ///< Mithörton an oder aus
    uint8_t SidetoneEnabled: 1;             ///< Mithörton ein- oder ausgeschaltet
    uint8_t SendStatus;                     ///< Status, ob Symbol gesendet wird
    uint8_t Automatic;                      ///< Speicher wird gesendet
    uint8_t LastSymbolWasDit: 1;            ///< letztes Symbol war ein Punkt
    uint8_t DitPressed: 1;                  ///< Dit Hebel betätigt
    uint8_t DahPressed: 1;                  ///< Dah Hebel betätigt
    uint8_t KeyState:1;                     ///<
    uint8_t KeyTX:1;                        ///<
};

struct MenuCtrl
{
    uint8_t  buttonPressed: 1;              ///< Taster Drehencoder gedrückt
    uint8_t  buttonPressedLong: 1;          ///< Taster Drehencoder lange gedrückt
    uint8_t  m_buttonPressed: 2;            ///< Merker für Taster Drehencoder gedrückt
    uint8_t  m_buttonPressedLong: 1;        ///< Merker für Taster Drehencoder lange gedrückt
    uint8_t  ClrScr: 1;                     ///< Bitmerker für Display löschen
    uint8_t  Config: 1;                     ///< Bitmerker für Config geändert
    uint8_t  Update: 1;                     ///< Bitmerker für Display update
    uint8_t  CurMenue;                      ///< aktuelles Menue
    uint8_t  SubMenue: 1;                   ///< im Submenue
};

struct Config
{
    uint8_t Trx1: 1;                        ///< TRX 1 eingeschaltet
    uint8_t Trx2: 1;                        ///< TRX 2 eingeschaltet
    uint8_t KeyerMode: 3;                   ///< Iambic A, Iambic B oder Ultimatic
    uint8_t SidetoneEnabled: 1;             ///< Mithörton eingeschaltet
    uint8_t WpMBpM: 1;                      ///< WpM oder BpM Anzeige
    uint8_t Reverse: 1;                     ///< linkes/rechtes Paddle vertauschen
    uint8_t Ratio;                          ///< Punkt/Strich Verhältnis 1:3
    uint8_t Weight;                         ///< Punkt/Strich Gewichtung
    uint8_t Memory:1;                       ///< Punkt/Strich Speicher
    uint8_t MemButtonMode:1;                ///< Button 5 als Umschalter TRX oder Speichertaste
    uint16_t SidetoneFreq;                  ///< Frequenz des Mithörtons
    uint8_t WpM;                            ///< WpM
    uint8_t RiseTime;                       ///< Anstiegszeit Sinuston
    uint8_t RiseTimeCounter;                ///< Anzahl Sinusschwingungen für den Anstieg
    uint8_t DebounceTime;                   ///< Entprellzeit für Straight Key Eingang
};

#define MSGSIZE 60                          ///< 60 Zeichen für jeden Textspeicher

struct Message  
{
    char Msg1[MSGSIZE];                     ///< Textspeicher 1
    char Msg2[MSGSIZE];                     ///< Textspeicher 2
    char Msg3[MSGSIZE];                     ///< Textspeicher 3
    char Msg4[MSGSIZE];                     ///< Textspeicher 4
    char Msg5[MSGSIZE];                     ///< Textspeicher 5
};



// Pin Change Interrupt Merker für Port 
uint8_t LastPINDState;

// Drehencoder
volatile uint8_t EncoderTimer;              ///< 10ms Timer for Encoder
volatile int8_t EncoderCounter;             ///< Vor/Rück Zähler
volatile int8_t EncoderPos;                 ///< Encoderposition für WpM/BpM
volatile int8_t EncoderPosConfig;           ///< Encoderposition für Einstellungen
volatile int8_t EncoderPosSubConfig;        ///< Encoderposition für Submenues
volatile uint16_t StoreEEpromTimer;         ///< Zählvariable für auto. Speicherung EEprom

uint8_t WpM;                                ///< Aktuelle Zeichengeschwindigkeit
uint8_t PaddleMode;                         ///< Merker für Links / Rechts vertauscht
uint8_t KeyerMode;                          ///< Merker für Iambic A, Iambic B oder Ultimatic
uint8_t KeyTX;

// Sidetone generation
volatile uint8_t icnt;
volatile uint8_t ocr2a;
volatile uint8_t lastButton;                    ///< Wert der letzten Buttonabfrage
volatile uint16_t DitMillis;                    ///< Dauer eines Dits
volatile uint16_t DahMillis;                    ///< Dauer eines Dahs
volatile uint16_t SpcMillis;                    ///< Dauer einer Pause zwischen den Zeichen
// Zähler und Status für Entprellung der Tasteneingänge und Mithörton
volatile uint8_t    StateStraightKeyPressed;    ///< Merker für StraightKey betätigt
volatile uint8_t    TimerStraightKeyPressed;    ///< Timer Variable für Entprellung
volatile uint8_t    StatePaddleDitKeyPressed;   ///< Merker für Paddle betätigt
volatile uint8_t    TimerPaddleDitKeyPressed;   ///< Timer Variable für Entprellung
volatile uint8_t    StatePaddleDahKeyPressed;   ///< Merker für Paddle betätigt
volatile uint8_t    TimerPaddleDahKeyPressed;   ///< Timer Variable für Entprellung
volatile uint8_t    StateButtonPressed;         ///< Merker für Speichertaste betätigt
volatile uint8_t    TimerButtonPressed;         ///< Timer Variable für Entprellung 
volatile uint8_t    StateRiseTimeCounter;       ///< Zähler für Anstieg des Mithörtons
volatile uint8_t    StateRiseTime;              ///< Timer für Anstieg des Mithörtons
// Diverse Zähler für Timer 0
uint16_t MenuCtrlTimer;                         ///< Wartezeit bis zur Betriebsanzeige nach Config
volatile uint16_t t_delayms;                    ///< Timer 0 max. 65535ms, all purpose timer variable
volatile uint16_t t_elementlength;              ///< Timer 0 max. 65535ms, element length of dit or dah
#endif 
