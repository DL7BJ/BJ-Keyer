/** @file   morse.c
 *  @brief  Funktionen für die Ausgabe von Speichertexten
 *  @author Tom, DL7BJ
 */

#include "globals.h"
#include "functions.h"
// Textspeicher
char ee_Msg1[MSGSIZE] EEMEM;
char ee_Msg2[MSGSIZE] EEMEM;                         
char ee_Msg3[MSGSIZE] EEMEM;
char ee_Msg4[MSGSIZE] EEMEM;
char ee_Msg5[MSGSIZE] EEMEM;

extern void SendChar(unsigned char c);
extern void SendString(char *s);

/** @fn     SetStandardMsg(void)
 *  @brief  Schreibt Standardnachrichten in den EEprom Speicher
 *          wenn dieses leer ist (nach dem Flashen des Controllers)
 *  @param  none
 *  @return none
 */
void SetStandardMsg(void)
{
//                    1         2         3         4         5         6 
//           123456789012345678901234567890123456789012345678901234567890
    sprintf(bMessage.Msg5,
            "BJ-KEYER by DL7BJ");
    sprintf(bMessage.Msg4,
            "CQ CQ CQ DE DL7BJ DL7BJ DL7BJ PSE K");
    sprintf(bMessage.Msg3,
            "NAME IS TOM TOM = QTH NR OLDENBURG OLDENBURG");
    sprintf(bMessage.Msg2,
            "QSL VIA BUREAU OK = TNX FER NICE QSO = 73");
    sprintf(bMessage.Msg1,
            "CQ CQ CQ RBN TEST DE DL7BJ DL7BJ DL7BJ PSE NO QSO");
}

/** @fn     void SendMemory(uint8_t MemoryIndex)
 *  @brief  Sendet den Text eines Speichers
 *
 *  @param  Nummer des Speichers
 *  @return none
 */
void SendMemory(uint8_t MemoryIndex)
{
    switch(MemoryIndex)
    {
        case 1:
            SendString(bMessage.Msg1);
        break;
        case 2:
            SendString(bMessage.Msg2);
        break;
        case 3:
            SendString(bMessage.Msg3);
        break;
        case 4:
            SendString(bMessage.Msg4);
        break;
        case 5:
            SendString(bMessage.Msg5);
        break;
    }
}
/** @fn     void WriteMsgEEprom(uint8_t idx)
 *  @brief  Textspeicher in EEprom schreiben
 *  @param  Nummer des Textspeichers
 *  @return none
 */
void WriteMsgEEprom(uint8_t idx)
{
    switch(idx)
    {
        case 1:
            eeprom_write_block(bMessage.Msg1,ee_Msg1,MSGSIZE);
        break;
        case 2:
            eeprom_write_block(bMessage.Msg2,ee_Msg2,MSGSIZE);
        break;
        case 3:
            eeprom_write_block(bMessage.Msg3,ee_Msg3,MSGSIZE);
        break;
        case 4:    
            eeprom_write_block(bMessage.Msg4,ee_Msg4,MSGSIZE);
        break;
        case 5:
            eeprom_write_block(bMessage.Msg5,ee_Msg5,MSGSIZE);
        break;
    }
}
/** @fn     void ReadMsgEEprom(uint8_t idx)
 *  @brief  Textspeicher aus EEprom lesen
 *  @param  Nummer des Textspeichers
 *  @return none
 */
void ReadMsgEEprom(uint8_t idx)
{
    switch(idx)
    {
        case 1:
            eeprom_read_block(bMessage.Msg1,ee_Msg1,MSGSIZE);
        break;
        case 2:
            eeprom_read_block(bMessage.Msg2,ee_Msg2,MSGSIZE);
        break;
        case 3:
            eeprom_read_block(bMessage.Msg3,ee_Msg3,MSGSIZE);
        break;
        case 4:
            eeprom_read_block(bMessage.Msg4,ee_Msg4,MSGSIZE);
        break;
        case 5:
            eeprom_read_block(bMessage.Msg5,ee_Msg5,MSGSIZE);
        break;
    }
}
