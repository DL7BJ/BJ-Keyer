/** @file   controls.h
 *  @brief  Headerdatei für controls.c
 *  @author Tom, DL7BJ
 */
#ifndef CONTROLS_H_INCLUDED
#define CONTROLS_H_INCLUDED

#include "globals.h"
#include "encoder.h"

// Externe Variablen
extern struct Config bConfig;
extern struct MenuCtrl bMenuCtrl;
extern struct State bState;
extern uint16_t MenuCtrlTimer;
extern uint8_t WpM;
// Externe Funktionen
extern void Boop(void);
extern void Beep(void);
extern void WriteEEprom(void);
extern void SideToneOff(void);
extern void DelayMilliSeconds(uint16_t t);
extern void Tone(uint16_t f, uint8_t t);
extern void SetRatio(void);
extern void SetWeight(void);
extern void SidetoneDisable(void);
extern void SidetoneEnable(void);
extern void SidetoneOff(void);
// Prototypes
void Drehencoder(void);
void ConfigMenue(void);
void UpdateDisplay(void);
void DisplayVersion(void);

#endif
