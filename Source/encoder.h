/** @file   encoder.h
 *  @date   2014-12-04
 *  @author Frank Klee
 *  @brief  Headerdatei Drehencoder Library
 *
 *          Basisroutinen zum Abfragen eines Drehencoders mittels Polling.
 *          Quelle: https://www.mikrocontroller.net/articles/Drehgeber
 *
 *  @brief  Formatierungen und Umbenennungen der Funktionen
 *  @author Tom, DL7BJ
 *  @date   2023-03-23
 */
#ifndef ENCODER_H_
#define ENCODER_H_
// Art des Drehencoders definieren
// #define SingleStep
// #define TwoStep
#define TwoStep                      ///< Art des Drehencoders

#define ENC_A_PORT      PORTB       ///< port for line A         
#define ENC_A_PIN       PB1         ///< pin  for line A         
#define ENC_B_PORT      PORTB       ///< port for line B         
#define ENC_B_PIN       PB0         ///< pin  for line B         
#define ENC_T_PORT      PORTB       ///< port for button         
#define ENC_T_PIN       PB2         ///< pin  for button         

#define DDR(x) (*(&x-1))            ///< Makro Datenrichtungsregister
#define Port(x) (*(&x))             ///< Makro Port
#define PIN(x) (*(&x-2))            ///< Makro Pin

#define PHASE_A		PIN(ENC_A_PORT) & (1<<ENC_A_PIN)        ///< Makro für Encoder PHASE A
#define PHASE_B     PIN(ENC_B_PORT) & (1<<ENC_B_PIN)        ///< Makro für Encoder PHASE B
#define BUTTONPRESSED (!(PIN(ENC_T_PORT) & (1<<ENC_T_PIN))) ///< Makro für Taster des Encoders

#define BUTTON_DEBOUNCETIME_MS  30                          ///< Entprellzeit Taster 
#define BUTTON_PRESSEDLONG_MS   250                         ///< Definition langer Tastendruck

typedef enum EButtonPressedState
{
    ButtonPressed_Unpressed,                                ///< kein Taster betötigt
    ButtonPressed_Short,                                    ///< Taster kurz betätigt
    ButtonPressed_Long                                      ///< Taster lang betätigt
}tEButtonPressedState;

// Initialisiert den Encoder und aktiviert den Interrupt + Timer
void EncoderInit( void );
// Liest die Position des Encoders aus
// Wenn Ueberlauf=1 dann zдhlt der Encoder nach Max
// wieder von Min und umgekehrt
int8_t EncoderRead(char Ueberlauf);
// Ruft den Status des Encoder-Knopfes
tEButtonPressedState EncoderGetButtonState(void);
// Setzt die aktuelle Drehencoderposition
void EncoderWrite(int8_t EncoderPos);
// Setzt Min- und Max-Werte fьr die Drehgeberposition
void EncoderMinMax(int8_t EncoderMin,int8_t EncoderMax);
void EncoderPolling(void);
#endif /* ENCODER_H_ */

