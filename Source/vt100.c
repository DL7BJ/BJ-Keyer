/** @file   vt100.c
 *  @brief  VT100 Terminalausgabe
 */
#include "vt100.h"

/** @fn     void SerialMenue(void)
 *  @brief  Auswahlmenue über serielle Schnittstelle ausgeben
 *  @param  none
 *  @retval none
 */
void SerialMenue(void)
{
    SerialWriteString("(1) Einstellungen anzeigen\r\n");
    if(bState.WriteMsgEEprom)
        SerialWriteString("\033[5m(2) Texte speichern\033[m\r\n");
    else
        SerialWriteString("(2) Texte speichern\r\n");
    SerialWriteString("(3) Texte anzeigen\r\n");
    SerialWriteString("(4) Texte ändern\r\n");
    SerialWriteString("\r\nEingabe: ");
    bSerialState.Ausgabe = SERMENUE;
}
/** @fn     void SerialSetup(void)
 *  @brief  Aktuelle Einstellungen über serielle Schnittstelle ausgeben
 *
 *  @param  none
 *  @retval none
 */
void SerialSetup(void)
{   
    sprintf(out,"%s",CLRSCR);
    SerialWriteString(out);
    SerialWriteString("Einstellungen\r\n\r\n");
    sprintf(out,"WpM             : %i\r\n", bConfig.WpM);
    SerialWriteString(out);
    sprintf(out,"KeyerMode       : %i\r\n", bConfig.KeyerMode);
    SerialWriteString(out);
    sprintf(out,"SidetoneFreq    : %u\r\n", bConfig.SidetoneFreq);
    SerialWriteString(out);
    sprintf(out,"TRX1            : %i\r\n", bConfig.Trx1);
    SerialWriteString(out);
    sprintf(out,"TRX2            : %i\r\n", bConfig.Trx2);
    SerialWriteString(out);
    sprintf(out,"SidetoneEnabled : %i\r\n", bConfig.SidetoneEnabled);
    SerialWriteString(out);
    sprintf(out,"Ratio           : %i\r\n", bConfig.Ratio);
    SerialWriteString(out);
    sprintf(out,"WpMBpM          : %i\r\n", bConfig.WpMBpM);
    SerialWriteString(out);
    sprintf(out,"Reverse         : %i\r\n", bConfig.Reverse);
    SerialWriteString(out);
    sprintf(out,"Memory          : %i\r\n", bConfig.Memory);
    SerialWriteString(out);
    sprintf(out,"MemButtonMode   : %i\r\n", bConfig.MemButtonMode);
    SerialWriteString(out);
    sprintf(out,"RiseTime        : %i\r\n", bConfig.RiseTime);
    SerialWriteString(out);
    sprintf(out,"RiseTimeCounter : %i\r\n", bConfig.RiseTimeCounter);
    SerialWriteString(out);
    sprintf(out,"DebounceTime    : %i\r\n", bConfig.DebounceTime);
    SerialWriteString(out);
    SerialWriteString("\r\nTaste ...");
    bSerialState.Ausgabe = SERSETUP;
}
/** @fn     void SerialAbout(void)
 *  @brief  Versionsinformation über serielle Schnittstelle ausgeben
 *
 *  @param  none
 *  @retval none
 */
void SerialAbout(void)
{
    sprintf(out,"%s", CLRSCR);
    SerialWriteString(out);
    sprintf(out,"%s %s %s\r\n\r\n",PRG,VER,CALL);
    SerialWriteString(out);
}
/** @fn     void SerialReset(void)
 *  @brief  Löscht die Terminalanzeige und gibt Versionsinfo und Menue aus
 *
 *  @param  none
 *  @retval none
 */  
void SerialReset(void)
{
    SerialAbout();
    SerialMenue();
    bSerialState.Eingabe = SERMSG0;
    i = 0;
    o = 0;
}
/** @fn     void SerialReceive(char c)
 *  @brief  Wird vom UART Interrupt aufgerufen, wenn ein Zeichen empfangen wurde
 *
 *  @param  c Zeichen, das empfangen wurde
 *  @retval none
 */
void SerialReceive(char c)
{
    // CTRL Steuercodes
    if(c == 0x1a) SerialReset();
    if(c == 0x03) SerialReset();
    switch(bSerialState.Ausgabe)
    {
        case SERMENUE:
            switch(c)
            {
                case 0x30:
                break;
                case 0x31:
                    SerialSetup();
                break;
                case 0x32:
                    bState.WriteMsgEEprom = 2;
                break;
                case 0x33:
                    SerialMsgOut();
                break;
                case 0x34:
                    sprintf(out,"%s",CLRSCR);
                    SerialWriteString(out);
                    SerialWriteString("Welcher Speicher? (1-5)");
                    bSerialState.Eingabe = SERMSG0;
                    bSerialState.Ausgabe = SERMSGMENUE;
                break;
            }
        break;
        case SERMSG:
            SerialReset();
        break;
        case SERSETUP:
            SerialReset();
        break;
        case SERMSGMENUE:
            SerialMsgMenue(c);
        break;
        default:
            SerialReset();
    }
}
/** @fn     void SerialMsgOut(char c)
 *  @brief  Zeigt die 5 Textspeicher im Terminal an
 *
 *  @param  none
 *  @retval none
 */
void SerialMsgOut(void)
{
    sprintf(out,"%s",CLRSCR);
    SerialWriteString(out);
    SerialWriteString("Textspeicher\r\n\r\n");
    sprintf(out,"1: %s\r\n",bMessage.Msg1);
    SerialWriteString(out);
    sprintf(out,"2: %s\r\n",bMessage.Msg2);
    SerialWriteString(out);
    sprintf(out,"3: %s\r\n",bMessage.Msg3);
    SerialWriteString(out);
    sprintf(out,"4: %s\r\n",bMessage.Msg4);
    SerialWriteString(out);
    sprintf(out,"5: %s\r\n",bMessage.Msg5);
    SerialWriteString(out);
    bSerialState.Ausgabe = SERMSG;
    SerialWriteString("\r\nTaste ...");
}
/** @fn     void SerialMsgMenue(char c)
 *  @brief  Zeigt einen Textspeicher an, der geändert werden soll
 *
 *  @param  c Nummer des Textspeichers
 *  @retval none
 */
void SerialMsgMenue(char c)
{
    bSerialState.Ausgabe = SERMSGMENUE;
    if(bSerialState.Eingabe == SERMSG0)
    {
        switch(c)
        {
            case 0x31:
                sprintf(out,"\r\n%s\r\n",bMessage.Msg1);
                SerialWriteString(out);
                bSerialState.Eingabe = SERMSG1;
            break;
            case 0x32:
                sprintf(out,"\r\n%s\r\n",bMessage.Msg2);
                SerialWriteString(out);
                bSerialState.Eingabe = SERMSG2;
            break;
            case 0x33:
                sprintf(out,"\r\n%s\r\n",bMessage.Msg3);
                SerialWriteString(out);
                bSerialState.Eingabe = SERMSG3;
            break;
            case 0x34:
                sprintf(out,"\r\n%s\r\n",bMessage.Msg4);
                SerialWriteString(out);
                bSerialState.Eingabe = SERMSG4;
            break;
            case 0x35:
                sprintf(out,"\r\n%s\r\n",bMessage.Msg5);
                SerialWriteString(out);
                bSerialState.Eingabe = SERMSG5;
            break;
 
        }
        i = 0; inp[i] = 0x00;
    } else 
    if(bSerialState.Eingabe > SERMSG0)
    {
        if(c == '\r')
        {
            SerialWriteString("\r\n\r\nOk! Druecke ESC\r\n");
            bState.WriteMsgEEprom = 1;
            inp[i] = 0x00;
            switch(bSerialState.Eingabe)
            {
                case SERMSG1:
                    sprintf(bMessage.Msg1,"%s",inp);
                break;
                case SERMSG2:
                    sprintf(bMessage.Msg2,"%s",inp);
                break;
                case SERMSG3:
                    sprintf(bMessage.Msg3,"%s",inp);
                break;
                case SERMSG4:
                    sprintf(bMessage.Msg4,"%s",inp);
                break;
                case SERMSG5:
                    sprintf(bMessage.Msg5,"%s",inp);
                break;
            }
        } else if (c == 0x08) {
            if(i > 0) i--;
            SerialWriteChar(0x7f);
        } else {
            if(i < 60) 
            {
                if((c > 96) && (c < 123))
                    c = c - 32;
                inp[i++] = c;
            } else
                SerialWriteChar(0x08);
        }
    }
    if(c == 0x1b) SerialReset();
}
