var encoder_8h =
[
    [ "BUTTON_DEBOUNCETIME_MS", "encoder_8h.html#a4f62996f43e5f428dae04bf6583e6095", null ],
    [ "BUTTON_PRESSEDLONG_MS", "encoder_8h.html#a0faf2a568de603ff9e6867f6ba2a9f89", null ],
    [ "BUTTONPRESSED", "encoder_8h.html#aa0661a7570d43557a4cbade846808684", null ],
    [ "DDR", "encoder_8h.html#acbe348573e1b5adccdc7bcf157dc6425", null ],
    [ "ENC_A_PIN", "encoder_8h.html#a7d575adbcfeaadc13a6d6606a2ed0c1b", null ],
    [ "ENC_A_PORT", "encoder_8h.html#a954872ee0daca1b1a8d0140b4fc80b82", null ],
    [ "ENC_B_PIN", "encoder_8h.html#ab42063ad37138f2e94f87270f7826aa0", null ],
    [ "ENC_B_PORT", "encoder_8h.html#a4e6f46965f5ffba14b80a7c353313982", null ],
    [ "ENC_T_PIN", "encoder_8h.html#a16f8e311086450a7061f6890fe028e61", null ],
    [ "ENC_T_PORT", "encoder_8h.html#abf7f0aca01dd1389118a21ec76ae7c13", null ],
    [ "PHASE_A", "encoder_8h.html#ad214039f52b011ce2bd6c85ff98a981b", null ],
    [ "PHASE_B", "encoder_8h.html#ad7b96feed1e1c12515dad5e926b2c62e", null ],
    [ "PIN", "encoder_8h.html#ad7581da21e9fd6fd6f6920dd68d1c782", null ],
    [ "Port", "encoder_8h.html#aa229b4be775fd996225a45c52a7e8a6d", null ],
    [ "TwoStep", "encoder_8h.html#a4484973712e579e11ec1f9306d4dfa5c", null ],
    [ "tEButtonPressedState", "encoder_8h.html#aa386d5445a9db1fd904ac94b12be4e2e", null ],
    [ "EButtonPressedState", "encoder_8h.html#a1f1601ea239948684d4b2a2f830bca26", [
      [ "ButtonPressed_Unpressed", "encoder_8h.html#a1f1601ea239948684d4b2a2f830bca26a7eda9605623092fc9f64c89f6126092d", null ],
      [ "ButtonPressed_Short", "encoder_8h.html#a1f1601ea239948684d4b2a2f830bca26a48e867e4b957ba7ef1d30609694100c2", null ],
      [ "ButtonPressed_Long", "encoder_8h.html#a1f1601ea239948684d4b2a2f830bca26a82989ac86ef7500aa2cc10c79404ff1b", null ]
    ] ],
    [ "EncoderGetButtonState", "encoder_8h.html#a2de57e0e3c5f3e0ab176c11363a58bf1", null ],
    [ "EncoderInit", "encoder_8h.html#a80ec2c177aa9c2636f797ca881381827", null ],
    [ "EncoderMinMax", "encoder_8h.html#ab5afede50bbce8aae40f3d02be64a374", null ],
    [ "EncoderPolling", "encoder_8h.html#a660e5b11dce82f5b307f628def81d188", null ],
    [ "EncoderRead", "encoder_8h.html#a9ec6df4b13c9134f1c61546b718cd563", null ],
    [ "EncoderWrite", "encoder_8h.html#ac9e955efd05fd5f6579bb5e179b506c7", null ]
];