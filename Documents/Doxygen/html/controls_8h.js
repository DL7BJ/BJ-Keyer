var controls_8h =
[
    [ "Beep", "controls_8h.html#a2f616a93763b4bc437c4311d653c0491", null ],
    [ "Boop", "controls_8h.html#a0366eaeb4aab61562dcd5b24994d0103", null ],
    [ "ConfigMenue", "controls_8h.html#a582148370499a6b91171ed6134cc7041", null ],
    [ "DelayMilliSeconds", "controls_8h.html#a4e03330a0d63dbc5c8c0ce8a3ba10ff7", null ],
    [ "DisplayVersion", "controls_8h.html#a9dec45875121142b6466b9475691b40a", null ],
    [ "Drehencoder", "controls_8h.html#ac6dc8b683fdf1a5fc24b2a509279f887", null ],
    [ "SetRatio", "controls_8h.html#aa0e063cf32b840a3ed51ae9aa677e2be", null ],
    [ "SetWeight", "controls_8h.html#a7a31f60576d112eca1abddf447df8248", null ],
    [ "SidetoneDisable", "controls_8h.html#ae8011166e9324f9f3c6948249eb15fd1", null ],
    [ "SidetoneEnable", "controls_8h.html#a60f2f041b0803584ba5fc2e0a373ed50", null ],
    [ "SideToneOff", "controls_8h.html#a543d0f249e5b505063a706525f99ff3a", null ],
    [ "SidetoneOff", "controls_8h.html#ad82537f7ea58b4eb375952ce52615c4d", null ],
    [ "Tone", "controls_8h.html#a378d56bf0e68310b700bd4dbdc652803", null ],
    [ "UpdateDisplay", "controls_8h.html#ace58ee95637a8f220527dc8b20dbb662", null ],
    [ "WriteEEprom", "controls_8h.html#a586bc00ad3164def7ec76ac9bda4f1bc", null ],
    [ "bConfig", "controls_8h.html#a8c3e2d3135447df0d9e7057fea4ff5b0", null ],
    [ "bMenuCtrl", "controls_8h.html#a885b2af021946a60a0bd2a271711c17a", null ],
    [ "bState", "controls_8h.html#a44ef5c8aeac95b862dab43408c149f9a", null ],
    [ "MenuCtrlTimer", "controls_8h.html#a088c685d08447f6614b3aea76cd94582", null ],
    [ "WpM", "controls_8h.html#a28fa6cb1767abefbe9c197700410d946", null ]
];