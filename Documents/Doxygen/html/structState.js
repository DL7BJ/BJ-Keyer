var structState =
[
    [ "Automatic", "structState.html#a77084e27de545c8c9326e317a8c1edff", null ],
    [ "DahPressed", "structState.html#a76bff5a7c3dd312851c50afccd017b33", null ],
    [ "DitPressed", "structState.html#a0e3dcd5d2b54a5256fd71a1d956a2a0d", null ],
    [ "KeyState", "structState.html#abd2083729e1ccf082240af06a4619756", null ],
    [ "KeyTX", "structState.html#aa0bd07f4422e7c411be3c34b8e0e7dbf", null ],
    [ "LastSymbolWasDit", "structState.html#a0b10c2ed83f730ab25f808ca4159e0a1", null ],
    [ "SendStatus", "structState.html#ac9b34cc2b3cb4f8ffe789278d9b76616", null ],
    [ "SidetoneEnabled", "structState.html#aaebbf5da7337abd9a683f6fd3d63087d", null ],
    [ "SidetoneOff", "structState.html#a5e94242222ee7d6061fb36f5374f36b4", null ],
    [ "WpMChanged", "structState.html#a52ffc38efd1a9798d8d92014fe664e62", null ],
    [ "WriteEEprom", "structState.html#a0589ba8e59b2a9a246ce8d14f783577b", null ],
    [ "WriteMsgEEprom", "structState.html#acbc62fb74e564116927efe2f2147b8b9", null ],
    [ "WriteWpMEEprom", "structState.html#a0213cd5057520f3b652f3755dcdc0e70", null ]
];