var searchData=
[
  ['ratio_149',['Ratio',['../structConfig.html#ac8db90b71b36d0a49ef231e68fb42797',1,'Config::Ratio()'],['../controls_8c.html#aafb08180dd6b40c7ceb1f987d29e01aa',1,'Ratio():&#160;controls.c']]],
  ['readeeprom_150',['ReadEEprom',['../functions_8c.html#adf7f3486f66ae3386f330a5172dfdf52',1,'ReadEEprom(void):&#160;functions.c'],['../functions_8h.html#adf7f3486f66ae3386f330a5172dfdf52',1,'ReadEEprom(void):&#160;functions.c']]],
  ['readeepromwpm_151',['ReadEEpromWpM',['../functions_8c.html#a3435bb0409b2c91ab253f4bd4cdc03e1',1,'ReadEEpromWpM(void):&#160;functions.c'],['../functions_8h.html#a3435bb0409b2c91ab253f4bd4cdc03e1',1,'ReadEEpromWpM(void):&#160;functions.c']]],
  ['readmsgeeprom_152',['ReadMsgEEprom',['../main_8h.html#a8e9bfd3675045369fd2f33121ad25e8e',1,'memory.c']]],
  ['resetmilliseconds_153',['ResetMilliSeconds',['../functions_8c.html#a318f7eb176f7797e0166cbd652855a3c',1,'ResetMilliSeconds(void):&#160;functions.c'],['../functions_8h.html#a318f7eb176f7797e0166cbd652855a3c',1,'ResetMilliSeconds(void):&#160;functions.c']]],
  ['restart_154',['ReStart',['../main_8h.html#ab1f1ff64cfdd7f3c70e793afb9b0e3af',1,'ReStart(void):&#160;main.c'],['../main_8c.html#ab1f1ff64cfdd7f3c70e793afb9b0e3af',1,'ReStart(void):&#160;main.c']]],
  ['reverse_155',['Reverse',['../structConfig.html#a8be063e5e1e539a860e2714b30d66ecc',1,'Config']]],
  ['reverselr_156',['ReverseLR',['../controls_8c.html#a3aef3986a3ae6ca48eb4f92532d827ae',1,'controls.c']]],
  ['reverserl_157',['ReverseRL',['../controls_8c.html#a6fcaa55631cc1a96c082f6d8da9357a2',1,'controls.c']]],
  ['right_5fpaddle_158',['RIGHT_PADDLE',['../globals_8h.html#a2531fa489a13983ebf61c93e71ec43d5',1,'globals.h']]],
  ['risetime_159',['RiseTime',['../structConfig.html#a627e732da58565b3d28f99b82c6a5c54',1,'Config::RiseTime()'],['../controls_8c.html#aec62b46d086a27a9d99a82f846b449a5',1,'RiseTime():&#160;controls.c']]],
  ['risetimecounter_160',['RiseTimeCounter',['../structConfig.html#a5afac0757bcf374b784a1250501d0bb0',1,'Config']]]
];
