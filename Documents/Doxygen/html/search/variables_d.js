var searchData=
[
  ['ratio_384',['Ratio',['../structConfig.html#ac8db90b71b36d0a49ef231e68fb42797',1,'Config::Ratio()'],['../controls_8c.html#aafb08180dd6b40c7ceb1f987d29e01aa',1,'Ratio():&#160;controls.c']]],
  ['reverse_385',['Reverse',['../structConfig.html#a8be063e5e1e539a860e2714b30d66ecc',1,'Config']]],
  ['reverselr_386',['ReverseLR',['../controls_8c.html#a3aef3986a3ae6ca48eb4f92532d827ae',1,'controls.c']]],
  ['reverserl_387',['ReverseRL',['../controls_8c.html#a6fcaa55631cc1a96c082f6d8da9357a2',1,'controls.c']]],
  ['risetime_388',['RiseTime',['../structConfig.html#a627e732da58565b3d28f99b82c6a5c54',1,'Config::RiseTime()'],['../controls_8c.html#aec62b46d086a27a9d99a82f846b449a5',1,'RiseTime():&#160;controls.c']]],
  ['risetimecounter_389',['RiseTimeCounter',['../structConfig.html#a5afac0757bcf374b784a1250501d0bb0',1,'Config']]]
];
