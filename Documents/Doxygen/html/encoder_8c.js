var encoder_8c =
[
    [ "tEButtonState", "encoder_8c.html#aedeed12448cdc285bc8c21763ae9f1a0", null ],
    [ "EButtonState", "encoder_8c.html#a7e7392f1e24ef55450099d5e7d095c4b", [
      [ "ButtonState_Unpressed", "encoder_8c.html#a7e7392f1e24ef55450099d5e7d095c4baa6d3bd33730cea4d3d97c7bb97611d0f", null ],
      [ "ButtonState_Pressed", "encoder_8c.html#a7e7392f1e24ef55450099d5e7d095c4bab17e4582ae92a602546a5ed70116b344", null ],
      [ "ButtonState_Hold", "encoder_8c.html#a7e7392f1e24ef55450099d5e7d095c4ba1ed28d4e307eb4999f1deaf3fa07f4d9", null ],
      [ "ButtonState_Released", "encoder_8c.html#a7e7392f1e24ef55450099d5e7d095c4ba19d35c76d8d53f420fd3106123d67a16", null ]
    ] ],
    [ "EncodeRead", "encoder_8c.html#aaf6a92358a883c87b032984732655084", null ],
    [ "EncoderGetButtonState", "encoder_8c.html#a2de57e0e3c5f3e0ab176c11363a58bf1", null ],
    [ "EncoderInit", "encoder_8c.html#a80ec2c177aa9c2636f797ca881381827", null ],
    [ "EncoderMinMax", "encoder_8c.html#ab5afede50bbce8aae40f3d02be64a374", null ],
    [ "EncoderPolling", "encoder_8c.html#a660e5b11dce82f5b307f628def81d188", null ],
    [ "EncoderRead", "encoder_8c.html#a9ec6df4b13c9134f1c61546b718cd563", null ],
    [ "EncoderWrite", "encoder_8c.html#ac9e955efd05fd5f6579bb5e179b506c7", null ],
    [ "buttonPressed", "encoder_8c.html#a32833c1bd4da805038b020ba58750218", null ],
    [ "buttonState", "encoder_8c.html#a9031f1bbc45388b7f4d7a6d534114e64", null ],
    [ "DrehgeberMax", "encoder_8c.html#aa7c6a2827b1cc4fcb2c5ff33d8d05300", null ],
    [ "DrehgeberMin", "encoder_8c.html#af734dbab527768cb6207a4a5bd90bd1d", null ],
    [ "DrehgeberPosition", "encoder_8c.html#abdbce2c910e9ca583dbbf74a99d50d72", null ],
    [ "enc_delta", "encoder_8c.html#a3f5ba1853e96736d1ffcc756e9138849", null ],
    [ "iButtonDebounceCycles", "encoder_8c.html#a93ec6852319d923e71003f50ad073327", null ],
    [ "iButtonPressedCounter", "encoder_8c.html#a25f7e7becdadc23b1f28f301dbf0325d", null ],
    [ "iButtonPressedLongCycles", "encoder_8c.html#aa12d33a8833ab123443cfe09d8c385e8", null ],
    [ "last", "encoder_8c.html#a904a7151d59a45adae73c4e0a68b5a1f", null ]
];