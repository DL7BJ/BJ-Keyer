var structConfig =
[
    [ "DebounceTime", "structConfig.html#ace4e1403e52d3d19aa9f2470a13cfaa3", null ],
    [ "KeyerMode", "structConfig.html#aef4f69db097a750c80f3b719bc14f341", null ],
    [ "MemButtonMode", "structConfig.html#adfbadb3847cb30d90b89f2db5b4850e4", null ],
    [ "Memory", "structConfig.html#a471840b99ff046bf1404f8640a8193a8", null ],
    [ "Ratio", "structConfig.html#ac8db90b71b36d0a49ef231e68fb42797", null ],
    [ "Reverse", "structConfig.html#a8be063e5e1e539a860e2714b30d66ecc", null ],
    [ "RiseTime", "structConfig.html#a627e732da58565b3d28f99b82c6a5c54", null ],
    [ "RiseTimeCounter", "structConfig.html#a5afac0757bcf374b784a1250501d0bb0", null ],
    [ "SidetoneEnabled", "structConfig.html#a7d6dfda9db154b977769378223d56eff", null ],
    [ "SidetoneFreq", "structConfig.html#aa3910963f687f6cea42eb74a662f0504", null ],
    [ "Trx1", "structConfig.html#a69e7d809f90ded45fed3d7511f5145dd", null ],
    [ "Trx2", "structConfig.html#a4401df925b1dc135a26043c0809db461", null ],
    [ "Weight", "structConfig.html#ace909792695e3949303df7fc26668739", null ],
    [ "WpM", "structConfig.html#ad897d01be9484ac915e8d545c5937e16", null ],
    [ "WpMBpM", "structConfig.html#a663b40e9d018ada8d4b6b97b4b2f5be0", null ]
];