\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Hinweise zur Dokumentation}{5}{chapter.1}%
\contentsline {chapter}{\numberline {2}Funktionsübersicht}{7}{chapter.2}%
\contentsline {chapter}{\numberline {3}Grundlagen}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Betriebsarten eines Morse-Keyers}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Iambic A}{9}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Iambic B}{10}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Ultimatic}{10}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Gewichtung}{11}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Punkt/Strich Ratio}{12}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Punkt/Strich Speicher}{12}{subsection.3.1.6}%
\contentsline {subsection}{\numberline {3.1.7}Handtaste}{13}{subsection.3.1.7}%
\contentsline {chapter}{\numberline {4}Die Bedienung}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Bedienelemente}{15}{section.4.1}%
\contentsline {section}{\numberline {4.2}Menuestruktur}{15}{section.4.2}%
\contentsline {section}{\numberline {4.3}Einstellungen}{15}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Transceiversteuerung}{15}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Mithörton}{16}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Iambic und Ultimatic Mode}{16}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}Punkt/Strich Speicher}{17}{subsection.4.3.4}%
\contentsline {subsection}{\numberline {4.3.5}Links- und Rechtshänder}{17}{subsection.4.3.5}%
\contentsline {subsection}{\numberline {4.3.6}Punkt/Strich Verhältnis}{17}{subsection.4.3.6}%
\contentsline {subsection}{\numberline {4.3.7}Gewichtung}{17}{subsection.4.3.7}%
\contentsline {subsection}{\numberline {4.3.8}Geschwindigkeitsanzeige}{17}{subsection.4.3.8}%
\contentsline {subsection}{\numberline {4.3.9}Anstiegszeit Mithörton}{17}{subsection.4.3.9}%
\contentsline {subsection}{\numberline {4.3.10}Entprellung für Handtasten}{17}{subsection.4.3.10}%
\contentsline {section}{\numberline {4.4}Einstellen der Geschwindigkeit}{17}{section.4.4}%
\contentsline {section}{\numberline {4.5}Lautstärke Mithörton}{17}{section.4.5}%
\contentsline {chapter}{\numberline {5}Die Schaltung}{19}{chapter.5}%
\contentsline {section}{\numberline {5.1}Spannungsversorgung}{19}{section.5.1}%
\contentsline {section}{\numberline {5.2}Mikrocontroller ATMega328P}{19}{section.5.2}%
\contentsline {section}{\numberline {5.3}USB Controller FT230}{20}{section.5.3}%
\contentsline {section}{\numberline {5.4}Class D NF-Verstärker}{20}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Cauer-Filter}{21}{subsection.5.4.1}%
\contentsline {section}{\numberline {5.5}Class D NF-Verstärker}{21}{section.5.5}%
\contentsline {section}{\numberline {5.6}Beschreibung}{21}{section.5.6}%
\contentsline {chapter}{\numberline {6}Die Software}{23}{chapter.6}%
\contentsline {section}{\numberline {6.1}Timer 2}{23}{section.6.1}%
\contentsline {section}{\numberline {6.2}Timer 1}{23}{section.6.2}%
\contentsline {section}{\numberline {6.3}Timer 0}{23}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Timer einstellen}{23}{subsection.6.3.1}%
\contentsline {section}{\numberline {6.4}Sinus Mithörton durch Pulsweitenmodulation}{23}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Grundlagen}{24}{subsection.6.4.1}%
\contentsline {subsubsection}{\nonumberline Pulsweitenmodulation}{24}{subsubsection*.18}%
\contentsline {subsection}{\numberline {6.4.2}Sinustabelle}{27}{subsection.6.4.2}%
\contentsline {chapter}{\numberline {7}Entwicklungsumgebung}{29}{chapter.7}%
\contentsline {chapter}{\nonumberline Tabellen}{31}{chapter*.24}%
\contentsline {chapter}{\nonumberline Abbildungen}{33}{chapter*.25}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
